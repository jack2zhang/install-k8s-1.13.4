#!/bin/bash
IP=$1
ssh root@$IP mkdir -p /etc/kubernetes/ssl
scp *.pem root@$IP:/etc/kubernetes/ssl/
#scp ca.csr root@$IP:/etc/kubernetes/ssl/
scp encryption-config.yaml audit-policy.yaml root@$IP:/etc/kubernetes/

