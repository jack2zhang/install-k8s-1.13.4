#!/bin/bash
IP=$1
ssh root@$IP mkdir -p /etc/kubernetes/ssl
scp k8s-root-ca.pem root@$IP:/etc/kubernetes/ssl
scp -r kube-proxy.kubeconfig bootstrap.kubeconfig root@$IP:/etc/kubernetes/
# bootstrap.kubeconfig  kubelet.kubeconfig  kube-proxy.kubeconfig


