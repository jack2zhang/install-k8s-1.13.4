#!/bin/bash
node=(
k8s-node-1
k8s-node-2
)

export KUBE_APISERVER="https://127.0.0.1:6443"
#export BOOTSTRAP_TOKEN=$(head -c 16 /dev/urandom | od -An -t x | tr -d ' ')
#echo "Tokne: ${BOOTSTRAP_TOKEN}"

for node in ${node[@]};do
echo $node

BOOTSTRAP_TOKEN=`kubeadm token create --description kubelet-bootstrap-token --groups system:bootstrappers:$node --kubeconfig ~/.kube/config`
echo "Create kubelet bootstrapping kubeconfig..."
# 配置集群参数
kubectl config set-cluster kubernetes \
  --certificate-authority=/etc/kubernetes/ssl/k8s-root-ca.pem \
  --embed-certs=true \
  --server=https://127.0.0.1:6443 \
  --kubeconfig=$node-bootstrap.kubeconfig
# 配置客户端认证
kubectl config set-credentials kubelet-bootstrap \
  --token=${BOOTSTRAP_TOKEN} \
  --kubeconfig=$node-bootstrap.kubeconfig
# 配置关联
kubectl config set-context default \
  --cluster=kubernetes \
  --user=kubelet-bootstrap \
  --kubeconfig=$node-bootstrap.kubeconfig
# 配置默认关联
kubectl config use-context default --kubeconfig=$node-bootstrap.kubeconfig
done

echo "Create kube-proxy kubeconfig..."

kubectl config set-cluster kubernetes \
  --certificate-authority=k8s-root-ca.pem \
  --embed-certs=true \
  --server=${KUBE_APISERVER} \
  --kubeconfig=kube-proxy.kubeconfig
# 设置客户端认证参数
kubectl config set-credentials kube-proxy \
  --client-certificate=kube-proxy.pem \
  --client-key=kube-proxy-key.pem \
  --embed-certs=true \
  --kubeconfig=kube-proxy.kubeconfig
# 设置上下文参数
kubectl config set-context default \
  --cluster=kubernetes \
  --user=kube-proxy \
  --kubeconfig=kube-proxy.kubeconfig
# 设置默认上下文
kubectl config use-context default --kubeconfig=kube-proxy.kubeconfig 
