#!/bin/bash
IP=$1
ssh root@$IP mkdir -p /etc/etcd/ssl
scp *.pem root@$IP:/etc/etcd/ssl
ssh root@$IP chown -R etcd:etcd /etc/etcd/ssl
ssh root@$IP chmod -R 644 /etc/etcd/ssl/*
ssh root@$IP chmod 755 /etc/etcd/ssl

