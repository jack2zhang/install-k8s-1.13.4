#!/bin/bash
IP=$1
ssh root@$IP mkdir /etc/nginx/ -p
scp -r nginx.conf root@$IP:/etc/nginx/
scp -r nginx-proxy.service root@$IP:/etc/systemd/system/
ssh root@$IP "systemctl daemon-reload;systemctl start nginx-proxy;systemctl enable nginx-proxy"
