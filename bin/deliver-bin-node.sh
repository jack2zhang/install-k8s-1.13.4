#!/bin/bash
IP=$1
scp kubectl  kubelet  kube-proxy root@$IP:/usr/local/bin/
ssh root@$IP 'chmod 755 /usr/local/bin/kube* ;mkdir /var/lib/kube-proxy;mkdir /var/lib/kubelet'
