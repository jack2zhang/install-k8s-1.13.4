#!/bin/bash
IP=$1
scp kube-apiserver kubectl kube-controller-manager kube-scheduler root@$IP:/usr/local/bin/
ssh root@$IP chmod 755 /usr/local/bin/{kube-apiserver,kubectl,kube-controller-manager,kube-scheduler}
