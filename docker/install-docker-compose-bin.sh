#!/bin/bash
### DOCS
### https://docs.docker.com/compose/install/

wget -c http://test.wgmf.com/mirrors/docker-compose/docker-compose  -O /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
docker-compose -v
wget -c test.wgmf.com/mirrors/docker-compose/bash_completion.d/docker-compose -O /etc/bash_completion.d/docker-compose

