#!/bin/bash
#rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
#rpm -Uvh http://www.elrepo.org/elrepo-release-7.0-2.el7.elrepo.noarch.rpm
#yum --disablerepo="*" --enablerepo="elrepo-kernel" list available

# disable yum fastestmirror
sed -i '/enabled/s/1/0/' /etc/yum/pluginconf.d/fastestmirror.conf
rpm --import http://test.wgmf.com/mirrors/repo/elrepo/RPM-GPG-KEY-elrepo.org
wget -c http://test.wgmf.com/mirrors/repo/elrepo/elrepo.repo -O /etc/yum.repos.d/elrepo.repo
yum --disablerepo="*" --enablerepo="elrepo" install kernel-lt kernel-lt-devel -y
#rpm -e elrepo-release
rm -f /etc/yum.repos.d/elrepo.repo
#sed -i '/GRUB_DEFAULT/s/saved/0/g' /etc/default/grub
#grub2-mkconfig -o /boot/grub2/grub.cfg
grub2-set-default 0
