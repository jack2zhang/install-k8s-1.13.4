#!/bin/bash
# 18.06 docker version with kubernetes 1.13.x
yum remove docker docker-common docker-selinux docker-engine -y
yum install -y yum-utils device-mapper-persistent-data lvm2
#yum-config-manager \
#    --add-repo \
#    https://download.docker.com/linux/centos/docker-ce.repo
sudo yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
yum clean all
yum makecache fast
#yum list docker-ce --showduplicates | sort -r
#yum install -y docker-ce
#yum install docker-ce-18.03.0.ce-1.el7.centos
yum install docker-ce-18.06.2.ce-3.el7 -y
systemctl start docker
docker run hello-world

