#!/bin/bash
cp conf/daemon.json /etc/docker/
mkdir -p /etc/systemd/system/docker.service.d
cd docker.service.d
cp -r docker-dns.conf  docker-options.conf  flannel.conf /etc/systemd/system/docker.service.d
cd ../..
cp systemd/docker.service /etc/systemd/system/
systemctl daemon-reload
systemctl restart docker
systemctl enable docker
