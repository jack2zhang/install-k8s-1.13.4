#!/bin/bash
echo "----------------------------------------------------------------------"
echo "close some services"
{
systemctl disable auditd 
systemctl disable chronyd
systemctl disable firewalld
systemctl disable NetworkManager
systemctl disable postfix 
systemctl disable system-udev 
systemctl disable kdump 
systemctl disable tuned 
systemctl disable wpa_supplicant 
systemctl disable dbus
systemctl disable auditd 

}  > /dev/null 2>&1

#Disable SeLinux
echo "Disable SeLinux"
setenforce 0
if [ -s /etc/selinux/config ]; then
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
fi

#optimizer sshd_config
echo "optimizer sshd_config"
sed -i "s/#UseDNS yes/UseDNS no/" /etc/ssh/sshd_config
sed -i "s/^GSSAPIAuthentication yes/GSSAPIAuthentication no/" /etc/ssh/sshd_config
sed -i "s/^#ListenAddress 0.0.0.0/ListenAddress 0.0.0.0/" /etc/ssh/sshd_config

#close IPV6
echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6
echo 1 > /proc/sys/net/ipv6/conf/default/disable_ipv6
{
cat <<'XUNLEI'
net.ipv6.conf.all.disable_ipv6 = 1
net.ipv6.conf.default.disable_ipv6 = 1
XUNLEI
} > /etc/sysctl.d/disable_ipv6.conf

#add exec(x) Permissions
chmod +x /etc/rc.d/rc.local
#set git-completion
if [ -f /usr/share/doc/git-1.8.3.1/contrib/completion/git-completion.bash ]; then
	cp /usr/share/doc/git-1.8.3.1/contrib/completion/git-completion.bash ~/.git-completion.bash
	echo 'source ~/.git-completion.bash' >> ~/.bashrc
	source ~/.git-completion.bash
fi
#set optimizer for /etc/fstab.conf
sed -i 's/defaults/defaults,noatime/' /etc/fstab
mount -a

echo "lock some users"
{
passwd -l xfs
passwd -l news
passwd -l nscd
passwd -l dbus
passwd -l vcsa
passwd -l games
passwd -l nobody
passwd -l avahi
passwd -l haldaemon
passwd -l gopher
passwd -l ftp
passwd -l mailnull
passwd -l pcap
passwd -l mail
passwd -l shutdown
passwd -l halt
passwd -l uucp
passwd -l operator
passwd -l sync
passwd -l adm
passwd -l lp

} > /dev/null 2>&1

echo "set important files md5"
cat > /tmp/list << EOF
/bin/ping
/bin/finger
/usr/bin/who
/usr/bin/w
/usr/bin/locate
/usr/bin/whereis
/sbin/ifconfig
/bin/pico
/bin/vi
/usr/bin/vim
/usr/bin/which
/usr/bin/gcc
/usr/bin/make
/bin/rpm
/bin/ps
/bin/netstat
EOF

echo "###########################" >>/var/log/`hostname`.log
echo `date` >>/var/log/`hostname`.log
for i in `cat /tmp/list`
do
if [ -x $i ];then
md5sum $i >> /var/log/`hostname`.log
fi
done
rm -f /tmp/list


